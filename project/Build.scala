import sbt._
import Keys._

object Build extends Build {

  override lazy val settings = super.settings ++ Seq(
    scalacOptions ++= Seq("-unchecked", "-deprecation", "-Xlint"),
    organization := "me.lazyval",
    scalaVersion := "2.10.4",
    initialCommands in console := "import me.lazyval._"
  )

  // altering source path, since you don't want to replicate usual src/main ... stuff
  val exampleSourcePath = scalaSource in Compile := baseDirectory.value / "."

  lazy val root = Project(id = "root-project", base = file("."), settings = Project.defaultSettings ++ settings)

  lazy val example1 = Project(id = "example1", base = file("./examples/example1"), settings = Project.defaultSettings ++ settings ++ Seq(exampleSourcePath)) dependsOn root

  lazy val example2 = Project(id = "example2", base = file("./examples/example2"), settings = Project.defaultSettings ++ settings ++ Seq(exampleSourcePath)) dependsOn root
}